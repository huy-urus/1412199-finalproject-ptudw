var db = require('../db');
var hashPw = require('./hashpwModel');

module.exports = {
    add: async (user) => {
        var query = `INSERT INTO "User"(
            "FullName", "Address", "Email", "Activated", "RoleId", "PasswordHash", "Status")
            VALUES ($1, $2, $3, $4, $5, $6, $7)`;
        await db.query(query, [user.name, user.address, user.email, 'false', 3, user.pw, 1]);
    },
    getUser: async email => {
        var query = 'SELECT * FROM "User" WHERE "Email" = $1';
        var result = await db.query(query, [email]);
        if (result.rows.length > 0) {
            return result.rows[0];
        }
        return null;
    }
}