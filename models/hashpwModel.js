var sha = require('sha.js');
var hashLength = 64;

function hashWithSalt(str, salt) {
    var preHash = str + salt;
    var hash = sha('sha256').update(preHash).digest('hex');
    var pwHash = hash + salt;
    return pwHash;
}


module.exports = {
    getHashWithSalt: str => {
        var salt = Date.now().toString(16);
        var pwHash = hashWithSalt(str, salt);
        return pwHash;
    },
    comparePassword: (pwIn, pwSaved) => {
        var salt = pwSaved.substring(hashLength, pwSaved.length);
        var pwHash = hashWithSalt(pwIn, salt);
        return pwHash === pwSaved;
    }
}