var express = require('express');
var session = require('express-session');
var path = require('path');
const bodyParser = require('body-parser');
var pg = require('pg');
var router = require('./controllers');

var app = express();
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, '/public')));


app.use(bodyParser.json());
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(session({
    secret: 'slkasldasdnaslsaboahov',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}))

app.use('/', router.home);
app.use('/Home', router.home);
app.use('/Account', router.account);

var port = 9090;
app.listen(port, function () {
    console.log('server started on port ' + port);
});
