var express = require('express');
const bodyParser = require('body-parser');
var accountModel = require('../models/accountModel');
var hashPw = require('../models/hashpwModel');

var router = express.Router();
var urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get('/Register', async (req, res) => {
    res.render('Account/Register', {
        title: "Register",
    });

})

router.post('/Register', urlencodedParser, async (req, res) => {
    var user = {
        name: req.body.txtFullname,
        email: req.body.txtEmail,
        address: req.body.txtAddress,
        pw: hashPw.getHashWithSalt(req.body.txtPassword)
    }

    var userChecker = await accountModel.getUser(user.email);
    if (userChecker) {
        res.redirect('/Account/Register');
        return;
    }

    await accountModel.add(user);
    res.redirect('../')
})

router.get('/Login', async (req, res) => {
    res.render('Account/Login', {
        title: "Login",
    });

})

router.post('/Login', urlencodedParser, async (req, res) => {
    var email = req.body.txtEmail;
    var pw = req.body.txtPassword;

    //Check user
    var user = await accountModel.getUser(email);
    if (user === null) {
        res.redirect('/Account/Login');
        return;
    }

    //Compare password
    var match = await hashPw.comparePassword(pw, user.PasswordHash);
    if (match) {
        req.session.user = user;
        res.redirect('/')
    }
    else {
        res.redirect('/Account/Login');
    }

})

module.exports = router;
