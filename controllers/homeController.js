var express = require('express');
var categoryModel = require('../models/categoryModel');
var router = express.Router();

router.get('/', async (req, res) => {
    // var cats = await categoryModel.getAll();
    res.render('Home/Index', {
        title: "Home",
    });
})

module.exports = router;
