const { Pool } = require('pg');

var config = {
    user: 'postgres', //env var: PGUSER
    database: 'OnlineAuction', //env var: PGDATABASE
    password: '12345', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, //env var: PGPORT
    max: 10, // max number of clients in the pool
    idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};
var pool = new Pool(config);
module.exports = {
    query: (text, params) => pool.query(text, params),
}